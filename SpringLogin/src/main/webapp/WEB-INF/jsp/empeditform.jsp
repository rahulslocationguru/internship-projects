
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
  <title></title>
</head>
<body bgcolor="#a9a9a9" align="center">
<div align="center"><br/>
<h1>Edit Employee</h1>
<form:form method="post" action="/editsave">
  <table>

    <tr>
      <td>Designation:</td>
      <td><form:input path="designation"/></td>
    </tr>

    <tr>
      <td>Password :</td>
      <td><form:input path="password"/></td>
    </tr>

    <tr>
      <td>Name :</td>
      <td><form:input path="name"/></td>
    </tr>

    <tr>
      <td></td>
      <td><input type="submit" value="Edit Save"></td>
    </tr>

  </table>
</form:form>
</div>
</body>
</html>
