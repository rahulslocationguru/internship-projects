
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
  <title></title>
</head>
<body bgcolor="#b8860b" align="center">
<div align="center"><br/>
<h1>Employee List</h1><br/>
<table border="2" width="70%" cellpadding="2">
  <tr><th>ID</th><th>Designation</th><th>Password</th><th>Name</th><th>Edit</th><th>Delete</th></tr>
  <c:forEach var="emp" items="${list}">
    <tr>
      <td>${emp.id}</td>
      <td>${emp.designation}</td>
      <td>${emp.password}</td>
      <td>${emp.name}</td>
      <td><a href="editemp/${emp.id}">Edit</a> </td>
      <td><a href="deleteemp/${emp.id}">Delete</a> </td>
    </tr>
  </c:forEach>
</table><br/><br/>
<a href="empform" align="center">Add New Employee</a>&emsp;<a href="/" align="center">AppHome</a>
  </div>
</body>
</html>
