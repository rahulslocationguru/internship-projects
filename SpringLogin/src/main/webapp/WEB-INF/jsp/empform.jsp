
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
  <title></title>
</head>
<body bgcolor="#778899"><br/>
<h1 align="center">Add New Employee</h1>
<div align="center">
<form:form method="post" action="/save">
  <table>
    <tr>
      <td>Designation :</td>
      <td><form:input path="designation"/></td>
    </tr>
    <tr>
      <td>Password :</td>
      <td><form:input path="password"/></td>
    </tr>
    <tr>
      <td>Name :</td>
      <td><form:input path="name"/></td>
    </tr>
    <tr><br/><br/>
      <td></td>
      <br/><br/><td><input type="submit" value="Save"></td>
    </tr>

  </table>
</form:form>
</div>
</body>
</html>
