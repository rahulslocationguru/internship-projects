package com.login.controllers;


import com.login.beans.Employee;
import com.login.dao.EmployeeDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class LoginController {

    public static final Logger logger = LogManager.getLogger(LoginController.class.getName());

    @Autowired
    EmployeeDao employeeDao;


    @RequestMapping("/welcome")
    public ModelAndView showindex(){

        logger.info("#Welcome page initiated");

        return new ModelAndView("welcome","command",new Employee());
    }

    @RequestMapping("/login")
    public ModelAndView showlogin(){

        return new ModelAndView("login","command",new Employee());
    }

    @RequestMapping("/empform")
    public ModelAndView showform(){

        return new ModelAndView("empform","command",new Employee());
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("emp")Employee emp){
        employeeDao.save(emp);
        return new ModelAndView("redirect:/viewemp");
    }

    @RequestMapping("/viewemp")
    public ModelAndView viewemp(){

        List<Employee> list= employeeDao.getEmployees();

        return new ModelAndView("viewemp","list",list);
    }

    @RequestMapping(value="/editemp/{id}")
    public ModelAndView edit(@PathVariable int id){

        Employee emp=employeeDao.getEmpById(id);
        return new ModelAndView("empeditform","command",emp);
    }

    @RequestMapping(value = "/editsave", method=RequestMethod.POST)
    public ModelAndView editsave(@ModelAttribute("emp")Employee emp){

        employeeDao.update(emp);
        return new ModelAndView("redirect:/viewemp");
    }

    @RequestMapping(value="/deleteemp/{id}",method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable int id){

        employeeDao.delete(id);

        return new ModelAndView("redirect:/viewemp");
    }

    @RequestMapping("/loginmanager")
    public ModelAndView loginmanager(String designation,String password, HttpSession httpSession,RedirectAttributes redirectAttributes) {

        Employee employeeDetails=employeeDao.getEmployeeName(designation, password);

        if(employeeDetails!=null) {
            String empName = employeeDetails.getName();
            String empDesignation = employeeDetails.getDesignation();
            String empPassword = employeeDetails.getPassword();

            if(httpSession!=null){
                httpSession.invalidate();
            }
            else

                httpSession.setAttribute("empDetails",employeeDetails);


            if ((empDesignation.equals("admin") || empDesignation.equals("ADMIN") || empDesignation.equals("Admin")) && (empPassword.equals(password))) {

                redirectAttributes.addFlashAttribute("empName", empName);
                return new ModelAndView("redirect:/admin-home");
            } else if ((empDesignation.equals("manager") || empDesignation.equals("MANAGER") || empDesignation.equals("Manager")) && (empPassword.equals(password))) {

                redirectAttributes.addFlashAttribute("empName", empName);
                return new ModelAndView("redirect:/manager-home");
            } else if((empDesignation.equals("employee") || empDesignation.equals("EMPLOYEE") || empDesignation.equals("Employee")) && (empPassword.equals(password))) {

                redirectAttributes.addFlashAttribute("empName", empName);
                return new ModelAndView("redirect:/employee-home");
            }

            else{
                redirectAttributes.addFlashAttribute("empName", empName);
                return new ModelAndView("redirect:/others-home");
            }

        }

        else {
            redirectAttributes.addFlashAttribute("message", "Invalid Designation or Password");
            return new ModelAndView("redirect:/login");
        }
    }


    @RequestMapping(value ="/admin-home", method=RequestMethod.GET)
    public ModelAndView showAdmin(Model model){

        String empName = (String) model.asMap().get("empName");

        return new ModelAndView("admin", "message", "Welcome, "+empName);

    }

    @RequestMapping(value ="/manager-home", method=RequestMethod.GET)
    public ModelAndView showManager(Model model){

        String empName = (String) model.asMap().get("empName");

        return new ModelAndView("manager", "message", "Welcome, "+empName);

    }

    @RequestMapping(value ="/employee-home", method=RequestMethod.GET)
    public ModelAndView showEmployee(Model model){

        String empName = (String) model.asMap().get("empName");

        return new ModelAndView("employee", "message", "Welcome, "+empName);

    }

    @RequestMapping(value ="/login", method=RequestMethod.GET)
    public ModelAndView invalidLogin(Model model){

        String message = (String) model.asMap().get("message");

        return new ModelAndView("login", "message", message);

    }

    @RequestMapping(value ="/others-home", method=RequestMethod.GET)
    public ModelAndView othersLogin(Model model){

        String empName = (String) model.asMap().get("empName");

        return new ModelAndView("others", "message", "Welcome, "+empName);

    }

    @RequestMapping(value ="/logout", method=RequestMethod.GET)
    public ModelAndView logout(HttpSession httpSession){

        httpSession.invalidate();

        return new ModelAndView("logout", "message", "Logout Successfully !!!");

    }

}
