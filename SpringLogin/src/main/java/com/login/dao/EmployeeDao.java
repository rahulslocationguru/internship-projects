package com.login.dao;

import com.login.beans.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.*;

public class EmployeeDao {

    public static final Logger logger = LogManager.getLogger(EmployeeDao.class.getName());

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Employee employee1){

        //Insert data in table
        try (Session session = this.sessionFactory.openSession()) {

            Transaction transaction1 = session.beginTransaction();

            employee1.setId(employee1.getId());
            employee1.setDesignation(employee1.getDesignation());
            employee1.setPassword(employee1.getPassword());
            employee1.setName(employee1.getName());

            session.save(employee1);

            transaction1.commit();
        } catch (Exception e) {
            logger.error(e);
        }
    }


    public List<Employee> getEmployees() {

        List list = null;

        //Read all data from table
        try (Session session = this.sessionFactory.openSession()) {

            Transaction transaction2 = session.beginTransaction();
            Query query = session.createQuery("from com.login.beans.Employee ");
            list = query.list();

            transaction2.commit();

        } catch (Exception e) {
            logger.error(e);
        }

        return list;

    }

    //Update Employee
    public void update(Employee employee2) {

        try (Session session = this.sessionFactory.openSession()){
            Transaction transaction3 = session.beginTransaction();

            employee2.setDesignation(employee2.getDesignation());
            employee2.setPassword(employee2.getPassword());
            employee2.setName(employee2.getName());

            session.save(employee2);
            transaction3.commit();

        } catch (Exception e) {
            logger.error(e);
        }
    }

    Employee employee=null;

    //Get Employee by Id
    public Employee getEmpById(int id){

        try (Session session = this.sessionFactory.openSession()){
            Transaction transaction3 = session.beginTransaction();

            Query query = session.createQuery("from Employee where id= :id");
            query.setParameter("id", id);
            employee = (Employee) query.uniqueResult();
            transaction3.commit();

        } catch (Exception e) {
            logger.error(e);
        }

        return employee;
    }

    //Delete Employee by Id
    public int delete(int id){

        int value=0;
        try (Session session = this.sessionFactory.openSession()) {
            Transaction transaction4 = session.beginTransaction();

            Query query = session.createQuery("delete from Employee where id= :id ");
            query.setParameter("id", id);

            value=query.executeUpdate();

            transaction4.commit();

        }catch (Exception e) {
            logger.error(e);
        }

        return value;

    }

    public Employee getEmployeeName(String designation,String password){

        try (Session session = this.sessionFactory.openSession()){
            Transaction transaction3 = session.beginTransaction();

            Query query = session.createQuery("from Employee where designation= :designation and password= :password");
            query.setParameter("designation", designation);
            query.setParameter("password", password);
            employee = (Employee) query.uniqueResult();
            transaction3.commit();

        } catch (Exception e) {
            logger.error(e);
        }

        return employee;
    }
}
