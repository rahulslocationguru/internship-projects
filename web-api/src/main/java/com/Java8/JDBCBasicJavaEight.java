package com.Java8;

import java.sql.*;
import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JDBCBasicJavaEight implements DriverAction {

    //DriverAction interface overridden method
    @Override
    public void deregister() {
        // TODO Auto-generated method stub

        logger.info("Driver deregistered");
    }

    private static final Logger logger = LogManager.getLogger(JDBCBasicJavaEight.class.getName());

    public static void main(String[] args)  {

        LocalDateTime localDateTime = LocalDateTime.now();
        logger.info("JDBC Java8 Enhancement at Date and time: {}",localDateTime);

        //Database Connection using register Driver
        Driver driver=new org.postgresql.Driver();
        DriverAction driverAction=new JDBCBasicJavaEight();
        try {
            DriverManager.registerDriver(driver, driverAction);
        } catch (SQLException e1) {
            // TODO Auto-generated catch block
            logger.error(e1);
        }

        try( Connection connection=DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples","postgres","pgsql");
             Statement statement=connection.createStatement()){

            String a="One";

            switch(a) {

                //1. insert 10 rows in users table
                case "One": {

                    statement.addBatch("delete from users");
                    statement.addBatch("insert into users values(1,'Julius Caesar','cardiff',9765432123),(2,'Sherlock Holmes','manchester',9765432123),"
                            + "(3,'Rupert Grave','Leeds',9654321234),(4,'Jim Moriarty','Oxford',9543212345),"
                            + "(5,'John Watson','SouthHampton',9432123456),(6,'Molly Hooper','Newport',9321234567),"
                            + "(7,'Philip Anderson','Durham',9212345678),(8,'Irene Adler','Nottingham',9123456789),"
                            + "(9,'Charles Augustus','Liverpool',9012345678),(10,'Janine Hawkins','Canterbury',9881234567)");

                    int status[]=statement.executeBatch();

                    if(status.length>0) {
                        logger.info("insert 10 rows perform successfully");
                    }

                    else {

                        logger.warn("Wrong sql query or tablename,Insert operation unsuccessful");
                    }
                    a="Two";
                }

                //2. select all rows in users table
                case "Two":{

                    try(ResultSet resultSet1=statement.executeQuery("select * from users")){
                        logger.info("ID Name City ContactNo");
                        while(resultSet1.next()) {
                            logger.info("{} {} {} {}",resultSet1.getInt(1),resultSet1.getString("name"),resultSet1.getString(3),resultSet1.getLong(4));
                        }
                    }catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }

                    a="Three";
                }

                //3. select the first row from users table
                case "Three":{

                    try(ResultSet resultSet2=statement.executeQuery("SELECT * FROM users LIMIT 1;")){
                        logger.info("Fetching top row only");
                        logger.info("ID Name City ContactNo");
                        while(resultSet2.next()) {
                            logger.info("{} {} {} {}",resultSet2.getInt(1),resultSet2.getString("name"),resultSet2.getString(3),resultSet2.getLong(4));
                        }
                    }catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }

                    a="Four";
                }


                //4. update all rows in users table
                case "Four":{

                    int change=statement.executeUpdate("update users set city='Melbourne', contactno=9800110033" );

                    if(change>0) {
                        logger.info("Update all rows done successfully");
                    }

                    else {
                        logger.info("Wrong ID,sql query or tablename,Update all rows unsuccessful");
                    }

                    a="Five";
                }


                //5. update the first row in users table
                case "Five":{

                    int status=statement.executeUpdate("update users set name='ABC',city='Sydney',contactno=1234 where id=1;");

                    if(status>0) {
                        logger.info("Update first row perform successfully");
                    }

                    else {
                        logger.warn("Wrong ID,sql query or tablename,Update operation unsuccessful");
                    }

                    a="Four";
                }


                //6. delete the first row in users table
                case "Six":{

                    int status=statement.executeUpdate("delete from users where id=1");

                    if(status>0) {
                        logger.info("Delete First row successful");
                    }

                    else {

                        logger.warn("Wrong sql query or tablename,Delete operation unsuccessful");
                    }

                    a="Seven";
                }

                //7. delete all rows in users table
                case "Seven":{


                    int value=statement.executeUpdate("delete from users;");


                    if(value>0){
                        logger.info("Table content delete operation perform successfully");
                    }

                    else {
                        logger.warn("wrong table name or empty table" );
                    }

                }

            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        }

            try {
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                logger.error(e);
            }
        }

    }


