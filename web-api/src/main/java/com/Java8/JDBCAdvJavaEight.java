package com.Java8;

import java.sql.*;
import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class JDBCAdvJavaEight implements DriverAction {

    //DriverAction interface overridden method
    @Override
    public void deregister() {
        // TODO Auto-generated method stub
        logger.info("Driver deregistered");
    }

    private static final Logger logger = LogManager.getLogger(JDBCAdvJavaEight.class.getName());

    public static void main(String[] args){

        LocalDateTime localDateTime = LocalDateTime.now();
        logger.info("JDBC Java8 Enhancement at Date and time: {}",localDateTime);

        //Database Connection using register Driver
        Driver driver=new org.postgresql.Driver();
        DriverAction driverAction=new JDBCAdvJavaEight();
        try {
            DriverManager.registerDriver(driver, driverAction);
        } catch (SQLException e1) {
            // TODO Auto-generated catch block
            logger.error(e1);
        }

            String a="One";
            switch(a) {

                //1. insert 10 rows in users table
                case "One": {

                    try(Connection connection=DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples","postgres","pgsql");
                        Statement statement=connection.createStatement()){
                        statement.addBatch("delete from users");
                        statement.addBatch("insert into users values(1,'Julius Caesar','cardiff',9765432123),(2,'Sherlock Holmes','manchester',9765432123),"
                                + "(3,'Rupert Grave','Leeds',9654321234),(4,'Jim Moriarty','Oxford',9543212345),"
                                + "(5,'John Watson','SouthHampton',9432123456),(6,'Molly Hooper','Newport',9321234567),"
                                + "(7,'Philip Anderson','Durham',9212345678),(8,'Irene Adler','Nottingham',9123456789),"
                                + "(9,'Charles Augustus','Liverpool',9012345678),(10,'Janine Hawkins','Canterbury',9881234567)");

                    int status[]=statement.executeBatch();

                    if(status.length>0) {
                        logger.info("insert 10 rows perform successfully");
                    }

                    else{
                        logger.warn("Wrong sql query or tablename,Insert operation unsuccessful");
                    }

                    }catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }
                    a="two";
                }

                //2. Update & retrieve all rows in single query
                case "two": {

                    try(Connection connection=DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples","postgres","pgsql");
                        Statement statement=connection.createStatement();
                        ResultSet resultSet=statement.executeQuery("update users set city='Newyork', contactno=9800110033 RETURNING *" )){
                    logger.info("Updated and retrieve table ");
                    logger.info("ID Name City ContactNo");
                    while(resultSet.next()) {
                        logger.info("{} {} {} {}",resultSet.getInt(1),resultSet.getString("name"),resultSet.getString(3),resultSet.getLong(4));
                    }
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }

                }

            }

            try {
                DriverManager.deregisterDriver(driver);
            }catch (SQLException e) {
                // TODO Auto-generated catch block
                logger.error(e);
            }
        }
    }

