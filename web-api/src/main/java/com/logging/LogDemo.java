package com.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogDemo {

    private static final Logger logger = LogManager.getLogger(LogDemo.class.getName());

    public static void main(String[] args){

        String fatal="ABC XYZ";
        String errorMsg="This is ERROR message";
        String crashMsg="System Crash";

        logger.debug("Hello world");
        logger.info("Logging in user {} with hashcode {}", logger.getName(), logger.hashCode());

        logger.warn("Unauthorized Access");
        logger.error("Message: "+errorMsg);    //without placeholder parameter
        logger.error("Message: {}", errorMsg); //with placeholder parameter

        logger.fatal("Alert: "+crashMsg);
        logger.fatal("Alert: {}",crashMsg);
    }
}
