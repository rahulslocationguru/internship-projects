package com.CPHikari;

import java.sql.*;
import java.time.*;
import java.time.chrono.JapaneseDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.*;

import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JavaEightEnhancedHikariCP {

    private static final Logger logger = LogManager.getLogger(JavaEightEnhancedHikariCP.class.getName());


    interface Printable{
        default void print()
        {
            logger.info("This is default Printable method");
        }

        public void printer();
    }

    public static void staticData()
    {
        logger.info("this is static method data in JavaEightEnhancedDemo class");
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        //Get DataSource from Connection Pool
        DataSource dataSource = CPConnector.getDatasource();

        //1. Lambda Expressions
        String print="Lambda expressions demo for printable interface";
        Printable printable1=()->{

            logger.info("This is : {}",print);
        };

        printable1.printer();

        //method references
        Printable printable2=JavaEightEnhancedHikariCP::staticData;
        printable2.print();


        //2. Collections implementation of java.util.stream
        List<Vehicle> vehicleList=new ArrayList<>();
        vehicleList.add(new Vehicle(1, "Audi", 70000));
        vehicleList.add(new Vehicle(2, "BMW", 50000));
        vehicleList.add(new Vehicle(3, "Porsche", 99000));
        vehicleList.add(new Vehicle(4, "Ferrari", 60000));
        vehicleList.add(new Vehicle(5, "Lamborghini",91000));
        List<Double> pricesList =vehicleList.stream().filter(p ->p.price> 60000)
                .map(pm ->pm.price).collect(Collectors.toList());

        logger.info("Filtered output Vehicle price above 60k: {}",pricesList);


        //3. Date-Time Package
        //LocalDateTime example
        LocalDateTime localDateTime = LocalDateTime.now();
        logger.info("{}",localDateTime);

        //TemporalAdjusters example first Day of month
        LocalDate localDateNow=LocalDate.now();
        LocalDate localDateFirstDay=localDateNow.with(TemporalAdjusters.firstDayOfMonth());
        logger.info("TemporalAdjusters ex First Day Of Month {}",localDateFirstDay);

        //DateTimeFormatter example
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dateformat=localDateNow.format(dateTimeFormatter);
        logger.info("Date formatter ex {}",dateformat);

        //ZoneDatetime class included in Java8 example
        LocalDateTime  customlocalDateTime = LocalDateTime.of(2018, Month.JANUARY,1,15,26);
        ZoneId  india = ZoneId.of("Asia/Kolkata");
        ZonedDateTime zoneDateTime=ZonedDateTime.of(customlocalDateTime, india);
        logger.info("{}",zoneDateTime);

        //java.time.chrono example representing calendar systems other than the default ISO
        JapaneseDate  japaneseDate=JapaneseDate.now();
        logger.info("{}",japaneseDate," Equals to \n\tISO-8601 Date",LocalDate.now(),"(based on the Gregorian calendar system)");



        //4. Enhancements in Packages java.lang.* and java.util.*

        //Parallel Array Sorting example uses multiple threads to sort improved speed
        int numArray[] = {70,50,40,30,10,20};
        logger.info("Parallel array sorting example");
        logger.info("Array before sort: {}",numArray);
        Arrays.parallelSort(numArray);
        logger.info("Array after parallel sort: {}",numArray);

        //Unsigned Arithmetic Support to Integer class
        String number=Integer.toUnsignedString(1265045);
        logger.info("Unsigned integer {} ",number," Unsigned Arithmetic Support added in Java8");

        //Standard Encoding and Decoding Base64
        logger.info("Base64 Encoder and Decoder Example");
        Base64.Encoder encoder=Base64.getEncoder();
        String encodedString=encoder.encodeToString("www.example.com".getBytes());
        logger.info("Encoded Url string {}",encodedString);

        Base64.Decoder decoder=Base64.getDecoder();
        String decodedString=new String(decoder.decode(encodedString));
        logger.info("Decoded Url string {}",decodedString);



        //5. JDBC Java8 Enhancements using Hikari Connection Pool

        try(Connection connection=dataSource.getConnection();
            Statement statement=connection.createStatement()){

            int value=statement.executeUpdate("delete from users");

            if(value>0) {
                logger.info("Delete rows perform successfully");
            }
            else {
                logger.warn("Table EMPTY or Wrong sql query or tablename,Delete operation unsuccessful");
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
        logger.error(e);
        }

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement=connection.prepareStatement("insert into users values(?,?,?,?)")
        ){

            preparedStatement.setInt(1, 11);
            preparedStatement.setString(2, "John Watson");
            preparedStatement.setString(3,"SouthHampton");
            preparedStatement.setInt(4,943212345);

            int value=preparedStatement.executeUpdate();
            if(value>0){
                logger.info("insert row perform successfully using Hikari Connection Pool");
            }

            else{
                logger.warn("Wrong sql query or tablename,Insert operation unsuccessful");
            }

        }catch(SQLException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        }

    }
}
