package com.CPHikari;

import java.sql.*;
import java.time.LocalDateTime;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JDBCBasicJavaCPHikari {

    private static final Logger logger = LogManager.getLogger(JDBCBasicJavaCPHikari.class.getName());

    public static void main(String[] args){

        LocalDateTime localDateTime = LocalDateTime.now();
        logger.info("Connection Pool Hikari run at Date and time: {}",localDateTime);

        //Get DataSource from Connection Pool Hikari
        DataSource dataSource = CPConnector.getDatasource();

        try(Connection connection=dataSource.getConnection();
            Statement statement=connection.createStatement()){

            int value=statement.executeUpdate("delete from users");

            if(value>0) {
                logger.info("Delete rows perform successfully");
            }
            else {
                logger.warn("Table EMPTY or Wrong sql query or tablename,Delete operation unsuccessful");
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        }

        try( Connection connection=dataSource.getConnection();
             Statement statement=connection.createStatement()){

            String a="One";

            switch(a) {

                //1. insert 10 rows in users table
                case "One": {

                    try(PreparedStatement preparedStatement=connection.prepareStatement("insert into users values(?,?,?,?)")){

                        preparedStatement.setInt(1, 1);
                        preparedStatement.setString(2, "Julius Caesar");
                        preparedStatement.setString(3,"cardiff");
                        preparedStatement.setInt(4,976543212);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,2);
                        preparedStatement.setString(2, "Sherlock Holmes");
                        preparedStatement.setString(3,"manchester");
                        preparedStatement.setInt(4,976543212);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,3);
                        preparedStatement.setString(2, "Rupert Grave");
                        preparedStatement.setString(3,"Leeds");
                        preparedStatement.setInt(4,965432123);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,4);
                        preparedStatement.setString(2, "Jim Moriarty");
                        preparedStatement.setString(3,"Oxford");
                        preparedStatement.setInt(4,954321234);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,5);
                        preparedStatement.setString(2, "John Watson");
                        preparedStatement.setString(3,"SouthHampton");
                        preparedStatement.setInt(4,943212345);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,6);
                        preparedStatement.setString(2, "Molly Hooper");
                        preparedStatement.setString(3,"Newport");
                        preparedStatement.setInt(4,932123456);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,7);
                        preparedStatement.setString(2, "Philip Anderson");
                        preparedStatement.setString(3,"Durham");
                        preparedStatement.setInt(4,921234567);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,8);
                        preparedStatement.setString(2, "Irene Adler");
                        preparedStatement.setString(3,"Nottingham");
                        preparedStatement.setInt(4,912345678);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,9);
                        preparedStatement.setString(2, "Charles Augustus");
                        preparedStatement.setString(3,"Liverpool");
                        preparedStatement.setInt(4,901234567);

                        preparedStatement.addBatch();

                        preparedStatement.setInt(1,10);
                        preparedStatement.setString(2, "Janine Hawkins");
                        preparedStatement.setString(3,"Canterbury");
                        preparedStatement.setInt(4,988123456);

                        preparedStatement.addBatch();

                        int status[]=preparedStatement.executeBatch();

                        if(status.length>0) {
                            logger.info("insert 10 rows perform successfully");
                        }

                        else {

                            logger.warn("Wrong sql query or tablename,Insert operation unsuccessful");
                        }

                        a="Beta";
                    }catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }
                    a="Two";
                }

                //2. select all rows in users table
                case "Two":{

                    try(ResultSet resultSet1=statement.executeQuery("select * from users")){
                        logger.info("ID Name City ContactNo");
                        while(resultSet1.next()) {
                            logger.info("{} {} {} {}",resultSet1.getInt(1),resultSet1.getString("name"),resultSet1.getString(3),resultSet1.getLong(4));
                        }
                    }catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }

                    a="Three";
                }

                //3. select the first row from users table
                case "Three":{

                    try(ResultSet resultSet2=statement.executeQuery("SELECT * FROM users LIMIT 1;")){
                        logger.info("Fetching top row only");
                        logger.info("ID Name City ContactNo");
                        while(resultSet2.next()) {
                            logger.info("{} {} {} {}",resultSet2.getInt(1),resultSet2.getString("name"),resultSet2.getString(3),resultSet2.getLong(4));
                        }
                    }catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }

                    a="Four";
                }


                //4. update all rows in users table
                case "Four":{

                    try(PreparedStatement preparedStatement=connection.prepareStatement("update users set city=?, contactno=? ")){

                        preparedStatement.setString(1, "Melbourne");
                        preparedStatement.setInt(2, 980011003);
                        int change = preparedStatement.executeUpdate();
                        if (change > 0) {
                            logger.info("Update all rows done successfully");
                        } else {
                            logger.info("Wrong ID,sql query or tablename,Update all rows unsuccessful");
                        }
                    }  catch (SQLException e) {
                            // TODO Auto-generated catch block
                            logger.error(e);
                    }

                    a="Five";
                }


                //5. update the first row in users table
                case "Five":{

                    try(PreparedStatement preparedStatement=connection.prepareStatement("update users set name=?,city=?,contactno=? where id=?;")) {


                        preparedStatement.setString(1, "ABC");
                        preparedStatement.setString(2,"Sydney");
                        preparedStatement.setInt(3,1234);
                        preparedStatement.setInt(4,1);

                        int status = preparedStatement.executeUpdate();
                        if (status > 0) {
                            logger.info("Update first row perform successfully");
                        } else {
                            logger.warn("Wrong ID,sql query or tablename,Update operation unsuccessful");
                        }

                    }  catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }

                    a="Four";
                }


                //6. delete the first row in users table
                case "Six":{

                    try(PreparedStatement preparedStatement=connection.prepareStatement("delete from users where id=?")){

                        preparedStatement.setInt(1, 1);
                        int status=preparedStatement.executeUpdate();

                    if(status>0) {
                        logger.info("Delete first row successful");
                    }

                    else {

                        logger.warn("Wrong sql query or tablename,Delete operation unsuccessful");
                    }
                    }  catch (SQLException e) {
                        // TODO Auto-generated catch block
                        logger.error(e);
                    }

                    a="Seven";
                }

                //7. delete all rows in users table
                case "Seven":{

                    int value=statement.executeUpdate("delete from users;");

                    if(value>0){
                        logger.info("Table content delete operation perform successfully");
                    }

                    else {
                        logger.warn("wrong table name or empty table" );
                    }

                }

            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        }


    }
}
