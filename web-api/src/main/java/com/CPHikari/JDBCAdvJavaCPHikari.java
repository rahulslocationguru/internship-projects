package com.CPHikari;

import java.sql.*;
import java.time.LocalDateTime;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JDBCAdvJavaCPHikari {

    private static final Logger logger = LogManager.getLogger(JDBCAdvJavaCPHikari.class.getName());

    public static void main(String[] args){

        LocalDateTime localDateTime = LocalDateTime.now();
        logger.info("Connection Pool Hikari run at Date and time: {}",localDateTime);

        //Get DataSource from Connection Pool Hikari
        DataSource dataSource = CPConnector.getDatasource();

        try(Connection connection=dataSource.getConnection();
            Statement statement=connection.createStatement()){

            int value=statement.executeUpdate("delete from users");

            if(value>0) {
                logger.info("Delete rows perform successfully");
            }
            else {
                logger.warn("Table EMPTY or Wrong sql query or tablename,Delete operation unsuccessful");
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        }

        String a="Alpha";
        switch(a) {

            //1. insert 10 rows in users table
            case "Alpha": {

                try(Connection connection=dataSource.getConnection();
                    PreparedStatement preparedStatement=connection.prepareStatement("insert into users values(?,?,?,?)")){

                    preparedStatement.setInt(1, 1);
                    preparedStatement.setString(2, "Julius Caesar");
                    preparedStatement.setString(3,"cardiff");
                    preparedStatement.setInt(4,976543212);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,2);
                    preparedStatement.setString(2, "Sherlock Holmes");
                    preparedStatement.setString(3,"manchester");
                    preparedStatement.setInt(4,976543212);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,3);
                    preparedStatement.setString(2, "Rupert Grave");
                    preparedStatement.setString(3,"Leeds");
                    preparedStatement.setInt(4,965432123);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,4);
                    preparedStatement.setString(2, "Jim Moriarty");
                    preparedStatement.setString(3,"Oxford");
                    preparedStatement.setInt(4,954321234);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,5);
                    preparedStatement.setString(2, "John Watson");
                    preparedStatement.setString(3,"SouthHampton");
                    preparedStatement.setInt(4,943212345);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,6);
                    preparedStatement.setString(2, "Molly Hooper");
                    preparedStatement.setString(3,"Newport");
                    preparedStatement.setInt(4,932123456);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,7);
                    preparedStatement.setString(2, "Philip Anderson");
                    preparedStatement.setString(3,"Durham");
                    preparedStatement.setInt(4,921234567);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,8);
                    preparedStatement.setString(2, "Irene Adler");
                    preparedStatement.setString(3,"Nottingham");
                    preparedStatement.setInt(4,912345678);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,9);
                    preparedStatement.setString(2, "Charles Augustus");
                    preparedStatement.setString(3,"Liverpool");
                    preparedStatement.setInt(4,901234567);

                    preparedStatement.addBatch();

                    preparedStatement.setInt(1,10);
                    preparedStatement.setString(2, "Janine Hawkins");
                    preparedStatement.setString(3,"Canterbury");
                    preparedStatement.setInt(4,988123456);

                    preparedStatement.addBatch();

                    int status[]=preparedStatement.executeBatch();

                    if(status.length>0) {
                        logger.info("insert 10 rows perform successfully");
                    }

                    else {

                        logger.warn("Wrong sql query or tablename,Insert operation unsuccessful");
                    }

                    a="Beta";
                }catch (SQLException e) {
                    // TODO Auto-generated catch block
                    logger.error(e);
                }
            }

            //2. Update & retrieve all rows in single query
            case "Beta": {

                try(Connection connection=dataSource.getConnection();
                   PreparedStatement preparedStatement=connection.prepareStatement("update users set city=?, contactno=? RETURNING *");
                    ){
                    preparedStatement.setString(1,"Newyork");
                    preparedStatement.setInt(2,980011003);
                    try(
                        ResultSet resultSet=preparedStatement.executeQuery()) {
                        logger.info("Updated and retrieve table ");
                        logger.info("ID Name City ContactNo");
                        while (resultSet.next())
                        {
                            logger.info("{} {} {} {}", resultSet.getInt(1), resultSet.getString("name"), resultSet.getString(3), resultSet.getLong(4));
                        }
                    }
                }catch (SQLException e) {
                    // TODO Auto-generated catch block
                    logger.error(e);
                }
            }
        }

    }
}
