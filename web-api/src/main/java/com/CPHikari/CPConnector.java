package com.CPHikari;

import javax.sql.DataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class CPConnector {

    private static DataSource datasource;

    public static DataSource getDatasource() {

        if(datasource==null) {

            HikariConfig hikariConfig=new HikariConfig();
            hikariConfig.setDataSourceClassName("org.postgresql.ds.PGPoolingDataSource");
            hikariConfig.addDataSourceProperty("url", "jdbc:postgresql://localhost:5432/internship_samples");
            hikariConfig.addDataSourceProperty("user", "postgres");
            hikariConfig.addDataSourceProperty("password", "pgsql");

            hikariConfig.setMaximumPoolSize(10);

            datasource = new HikariDataSource(hikariConfig);
        }
        return datasource;
    }
}

