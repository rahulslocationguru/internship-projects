package com.CPHikari;

public class Vehicle {

    int id;
    String name;
    double price;

    public Vehicle(int id, String name, double price) {

        this.id = id;
        this.name = name;
        this.price = price;
    }
}
