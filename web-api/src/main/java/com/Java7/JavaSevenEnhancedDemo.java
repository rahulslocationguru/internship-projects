package com.Java7;

import java.util.*;
import java.sql.*;
import java.io.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JavaSevenEnhancedDemo {

    private static final Logger logger = LogManager.getLogger(JavaSevenEnhancedDemo.class.getName());

    //Java 7 Enhancements
    @SuppressWarnings({ "unused"})  //Suppress warning Java 7 feature
    public static void main(String[] args) {

        String value = "Alpha";

        switch (value) {          //String used in switch case Java 7 feature

            //1. Demonstrate Binary Literals & Underscores in Numeric Literals
            //calculate area of rectangle
            case "Alpha": {

                int length = 0b110111;     //length=50    Binary Literals
                int breadth = 15_9;        //breadth=159  Underscores in Numeric Literals
                double area = length * breadth;
                logger.info("The area of rectangle is {}", area);

                value = "Beta";
            }

            //2. Demonstrate Type Inference for Generic Instance Creation
            case "Beta": {

                List<String> list = new ArrayList<>();  // In Java SE 7, we can substitute the parameterized type of the constructor
                list.add("Cricket");
                list.add("Football");
                list.add("Golf");
                list.add("Archery");
                list.add("Badminton");
                list.add("Hockey");

                Iterator<String> iterator = list.iterator();

                while (iterator.hasNext()) {
                    logger.info(iterator.next());
                }

            }

            //3. Demonstrate try with multi-catch & try with resources example
            case "charlie": {

                try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples", "postgres", "pgsql");
                     Statement statement = connection.createStatement();) {

                    FileInputStream fileInputStream = new FileInputStream("A1.log");
                    statement.addBatch("delete from users");
                    statement.addBatch("insert into users values(1,'Andy','Yorkshire',9765432123),(2,'Steve','Leeds',9765432123)");

                    int status[] = statement.executeBatch();

                    if (status.length > 0) {
                        logger.info("insert rows perform successfully");
                    } else {
                        logger.warn("Wrong sql query or tablename,Insert operation unsuccessful");
                    }

                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        logger.error(e);
                    }
                } catch (SQLException | FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    logger.error(e);

                }

            }

        }
    }
}
