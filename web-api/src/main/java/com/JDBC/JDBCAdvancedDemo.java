package com.JDBC;

import java.sql.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JDBCAdvancedDemo {

    private static final Logger logger = LogManager.getLogger(JDBCAdvancedDemo.class.getName());

    public static void main(String[] args){

        Connection connection = null; //connection initialized here to access in finally block
        Statement statement=null;
        ResultSet resultSet=null;

        //Database Connection
        try {
            Class.forName("org.postgresql.Driver");
            connection=DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples","postgres","pgsql");

            int a=1;
            switch(a) {

                //1. insert 10 rows in users table
                case 1: {

                    statement=connection.createStatement();
                    statement.addBatch("delete from users");
                    statement.addBatch("insert into users values(1,'Julius Caesar','cardiff',9765432123),(2,'Sherlock Holmes','manchester',9765432123),"
                            + "(3,'Rupert Grave','Leeds',9654321234),(4,'Jim Moriarty','Oxford',9543212345),"
                            + "(5,'John Watson','SouthHampton',9432123456),(6,'Molly Hooper','Newport',9321234567),"
                            + "(7,'Philip Anderson','Durham',9212345678),(8,'Irene Adler','Nottingham',9123456789),"
                            + "(9,'Charles Augustus','Liverpool',9012345678),(10,'Janine Hawkins','Canterbury',9881234567)");

                    int status[]=statement.executeBatch();

                    if(status.length>0) {
                        logger.info("insert 10 rows perform successfully");
                    }

                    else {

                        logger.warn("Wrong sql query or tablename,Insert operation unsuccessful");
                    }

                    a++;
                }

                //2. Update & retrieve all rows in single query
                case 2: {

                    statement=connection.createStatement();
                    resultSet=statement.executeQuery("update users set city='Newyork', contactno=9800110033 RETURNING *" );
                    logger.info("Updated and retrieve table ");
                    logger.info("ID Name City ContactNo");
                    while(resultSet.next()) {
                        logger.info("{} {} {} {}",resultSet.getInt(1),resultSet.getString("name"),resultSet.getString(3),resultSet.getLong(4));
                    }

                }

            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        }finally{

            try {
                if(resultSet!=null)
                {
                    resultSet.close();
                }
            }catch (SQLException e) {
                logger.error(e);
            }

            try {

                if(statement!=null) {
                    statement.close();
                }}catch (SQLException e) {
                logger.error(e);
            }

            try {
                if(connection!=null) {
                    connection.close();
                }}catch (SQLException e) {
                logger.error(e);
            }
            }
        }
    }

