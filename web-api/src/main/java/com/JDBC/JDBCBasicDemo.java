package com.JDBC;

import java.sql.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JDBCBasicDemo {

    private static final Logger logger = LogManager.getLogger(JDBCBasicDemo.class.getName());

    public static void main(String[] args)  {

        Connection connection = null; //connection initialized here to access in finally block
        Statement statement=null;
        ResultSet resultSet=null;
        PreparedStatement preparedStatement=null;

        try {

            //Database Connection
            Class.forName("org.postgresql.Driver");
            connection=DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples","postgres","pgsql");
            int a=1;

            switch(a) {

                //1. insert 10 rows in users table
                case 1: {

                    statement=connection.createStatement();

                    statement.addBatch("delete from users");
                    statement.addBatch("insert into users values(1,'Julius Caesar','cardiff',9765432123),(2,'Sherlock Holmes','manchester',9765432123),"
                            + "(3,'Rupert Grave','Leeds',9654321234),(4,'Jim Moriarty','Oxford',9543212345),"
                            + "(5,'John Watson','SouthHampton',9432123456),(6,'Molly Hooper','Newport',9321234567),"
                            + "(7,'Philip Anderson','Durham',9212345678),(8,'Irene Adler','Nottingham',9123456789),"
                            + "(9,'Charles Augustus','Liverpool',9012345678),(10,'Janine Hawkins','Canterbury',9881234567)");

                    int status[]=statement.executeBatch();

                    if(status.length>0) {
                        logger.info("insert 10 rows perform successfully");
                    }

                    else {

                        logger.warn("Wrong sql query or tablename,Insert operation unsuccessful");
                    }
                    a++;
                }


                //2. select all rows in users table
                case 2:{

                    statement=connection.createStatement();
                    resultSet=statement.executeQuery("select * from users");
                    logger.info("ID Name City ContactNo");
                    while(resultSet.next()) {
                        logger.info("{} {} {} {}",resultSet.getInt(1),resultSet.getString("name"),resultSet.getString(3),resultSet.getLong(4));
                    }

                    a++;
                }


                //3. select the first row from users table
                case 3:{

                    statement=connection.createStatement();
                    resultSet=statement.executeQuery("SELECT * FROM users LIMIT 1;");
                    logger.info("Fetching top row only");
                    logger.info("ID Name City ContactNo");
                    while(resultSet.next()) {
                        logger.info("{} {} {} {}",resultSet.getInt(1),resultSet.getString("name"),resultSet.getString(3),resultSet.getLong(4));
                    }

                    a++;
                }


                //4. update all rows in users table
                case 4:{

                    statement=connection.createStatement();
                    int change=statement.executeUpdate("update users set city='Melbourne', contactno=9800110033" );

                    if(change>0) {
                        logger.info("Update all rows done successfully");
                    }

                    else {
                        logger.info("Wrong ID,sql query or tablename,Update all rows unsuccessful");
                    }

                    a++;
                }

                //5. update the first row in users table
                case 5:{

                    statement=connection.createStatement();

                    int status=statement.executeUpdate("update users set name='ABC',city='Sydney',contactno=1234 where id=1;");

                    if(status>0) {
                        logger.info("Update perform successfully");
                    }

                    else {

                        logger.warn("Wrong ID,sql query or tablename,Update operation unsuccessful");
                    }

                    a++;
                }


                //6. delete the first row in users table
                case 6:{

                    preparedStatement=connection.prepareStatement("delete from users where id=?");
                    preparedStatement.setInt(1, 1);
                    int status=preparedStatement.executeUpdate();

                    if(status>0) {
                        logger.info("Delete First row successful");
                    }

                    else {

                        logger.warn("Wrong sql query or tablename,Delete operation unsuccessful");
                    }

                    a++;
                }

                //7. delete all rows in users table
                case 7:{

                    statement=connection.createStatement();
                    int value=statement.executeUpdate("delete from users;");


                    if(value>0){
                        logger.info("Table content delete operation perform succesfully");
                    }

                    else {
                        logger.warn("wrong table name or empty table" );
                    }

                    a++;
                }

            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error(e);
        }finally{
            try {
                if(resultSet!=null) {
                    resultSet.close();
                }}catch (SQLException e) {
                logger.error(e);
            }

            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }}catch (SQLException e) {
            logger.error(e);
        }

            try {

                if(statement!=null) {
                    statement.close();
                }}catch (SQLException e) {
                logger.error(e);
            }

            try {
                if(connection!=null) {
                    connection.close();
                }}catch (SQLException e) {
                logger.error(e);
            }
            }
        }
    }

