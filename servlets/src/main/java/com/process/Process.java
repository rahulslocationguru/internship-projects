package com.process;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@WebServlet(name = "Process")
public class Process extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(Process.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("doGet method called");

        String name=request.getParameter("name");

        PrintWriter out=response.getWriter();
        out.print("<html><body bgcolor='lightyellow'><div align='center'><br><br><h1>Hello, "+name+"</h1><br>" +
                  "<h2><a href='http://localhost:8080/index.jsp'>Home</a></h2></div></body></html>");

        logger.info("doGet method ends");

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doDelete(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
    }
}
